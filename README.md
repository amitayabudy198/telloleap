# TelloLeap

This project was an experiment to test how to control your DJI Tello Drone using the Leap Motion device.

## Behind the project
The idea behind this project is to make a cool controller to the tello drone. Your own hands.
Using the leap motion's abilitis to recognize hands gestures and movements I stated working on a way to use them with the tello.

## How to use it
To use the TelloLeap you'll need to download the Leap Motion sdk from https://developer.leapmotion.com/sdk/v2
Also, you'll need to use the djitellopy library. You can get it from https://github.com/damiafuentes/DJITelloPy

## The idea
My first goal to achieve is a working controller that uses both hands, one for steering and changing the height and another one for moving the drone itself.
I want to do it with as little lag as possible and very natural to use.
In the future I will want to try controling the drone only with one hand. For both movment and steering.
