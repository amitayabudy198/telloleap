import os, sys, inspect, thread, time
#if using windows and not mac change the next two lines to the lines afterwards in comment
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = os.path.abspath(os.path.join(src_dir, '../lib'))
"""
src_dir = os.path.dirname(inspect.getfile(inspect.currentframe()))
arch_dir = '../lib/x64' if sys.maxsize > 2**32 else '../lib/x86'
"""
sys.path.insert(0, os.path.abspath(os.path.join(src_dir, arch_dir)))

import Leap

from Leap import CircleGesture, KeyTapGesture, ScreenTapGesture, SwipeGesture

from djitellopy import Tello
import cv2
import time

	
tello = Tello()
tello.connect()
tello.takeoff()

class SampleListener(Leap.Listener):

	finger_names = ['Thumb', 'Index', 'Middle', 'Ring', 'Pinky']
	state_names = ['STATE_INVALID', 'STATE_START', 'STATE_UPDATE', 'STATE_END']

	def on_init(self, controller):
		print "Initialized"

	def on_connect(self, controller):
		print "Connected"

		# Enable gestures
		controller.enable_gesture(Leap.Gesture.TYPE_CIRCLE);
		controller.enable_gesture(Leap.Gesture.TYPE_KEY_TAP);
		controller.enable_gesture(Leap.Gesture.TYPE_SCREEN_TAP);
		controller.enable_gesture(Leap.Gesture.TYPE_SWIPE);

	def on_disconnect(self, controller):
		# Note: not dispatched when running in a debugger.
		print "Disconnected"

	def on_exit(self, controller):
		print "Exited"

	def control_direction(self, hand):
		# X - Left or Right. Left -1. Right 1. Center 0.
		# Y - Up or Down. Down -1. Up 1. Center 0.
		if hand.direction.x > 0.3:
			tello.rotate_clockwise(20)
		elif hand.direction.x < -0.3: 
			tello.rotate_counter_clockwise(20)

		if hand.direction.y > 0.3:
			tello.move_up(27)
		elif hand.direction.y < -0.3:
			tello.move_down(27)

	def control_movement(self, hand):
		# X - Left or Right. Left -1. Right 1. Center 0.
		# Y - Up or Down. Down -1. Up 1. Center 0.
		if hand.direction.x > 0.3:
			tello.move_right(20)
		elif hand.direction.x < -0.3: 
			tello.move_left(20)

		if hand.direction.y > 0.3:
			tello.move_back(20)
		elif hand.direction.y < -0.3:
			tello.move_forward(20)
			

	def on_frame(self, controller):
		# Get the most recent frame and report some basic information
		frame = controller.frame()
		hands = frame.hands

		for hand in hands:
			if hand.is_right:
				self.control_direction(hand)
			elif hand.is_left:
				self.control_movement(hand)

	def state_string(self, state):
		if state == Leap.Gesture.STATE_START:
			return "STATE_START"

		if state == Leap.Gesture.STATE_UPDATE:
			return "STATE_UPDATE"

		if state == Leap.Gesture.STATE_STOP:
			return "STATE_STOP"

		if state == Leap.Gesture.STATE_INVALID:
			return "STATE_INVALID"

def main():
	print "Left Hand - Controls movment. Right Hand - Controls direction"
	# Create a sample listener and controller
	listener = SampleListener()
	controller = Leap.Controller()


	# Have the sample listener receive events from the controller
	controller.add_listener(listener)

	# Keep this process running until Enter is pressed
	print "Press Enter to quit..."
	try:
		sys.stdin.readline()
	except KeyboardInterrupt:
		pass
	finally:
		# Remove the sample listener when done
		controller.remove_listener(listener)
		tello.land()
		tello.end()





if __name__ == "__main__":
	main()
